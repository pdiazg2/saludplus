<?php

namespace app\controllers;

use app\models\ModeloProductos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductosController implements the CRUD actions for ModeloProductos model.
 */
class ProductosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ModeloProductos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ModeloProductos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idProductos' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ModeloProductos model.
     * @param int $idProductos Id Productos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idProductos)
    {
        return $this->render('view', [
            'model' => $this->findModel($idProductos),
        ]);
    }

    /**
     * Creates a new ModeloProductos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ModeloProductos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idProductos' => $model->idProductos]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ModeloProductos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idProductos Id Productos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idProductos)
    {
        $model = $this->findModel($idProductos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idProductos' => $model->idProductos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ModeloProductos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idProductos Id Productos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idProductos)
    {
        $this->findModel($idProductos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ModeloProductos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idProductos Id Productos
     * @return ModeloProductos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idProductos)
    {
        if (($model = ModeloProductos::findOne(['idProductos' => $idProductos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
