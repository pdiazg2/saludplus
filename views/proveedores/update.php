<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProveedores $model */

$this->title = 'Update Modelo Proveedores: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-proveedores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
