<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloRealizan $model */

$this->title = 'Create Modelo Realizan';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Realizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-realizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
