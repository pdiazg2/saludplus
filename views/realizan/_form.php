<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloRealizan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-realizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProcedimiento')->textInput() ?>

    <?= $form->field($model, 'idMedico')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
