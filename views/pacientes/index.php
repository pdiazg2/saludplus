<?php

use app\models\ModeloPacientes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Modelo Pacientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-pacientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Modelo Pacientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'dni',
            'direccion',
            'apellido',
            'nombre',
            //'expediente',
            //'idMedico',
            //'fechaDeConsulta',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloPacientes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
