<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloPacientes $model */

$this->title = 'Create Modelo Pacientes';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-pacientes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
