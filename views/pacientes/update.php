<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloPacientes $model */

$this->title = 'Update Modelo Pacientes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-pacientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
