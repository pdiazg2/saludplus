<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonospaciente $model */

$this->title = 'Create Modelo Telefonospaciente';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Telefonospacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonospaciente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
