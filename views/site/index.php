<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
$this->title = 'Gestión de Centros de Salud';
?>

<style>
    .site-index {
        background-image: url('imagenes/fondo.jpg');
        background-size: cover;
        background-position: center center;
        background-attachment: fixed;
        height: 100vh;
    }

    .jumbotron {
        background-color: rgba(0, 0, 255, 0.5); /* Color azul transparente (ajusta el alpha según necesites) */
        padding: 20px;
        border-radius: 15px; /* Añade bordes redondeados al jumbotron */
    }

    .jumbotron h1, .jumbotron p, .jumbotron a {
        color: #fff; /* Cambia el color del texto a blanco para mejorar la legibilidad */
    }
    
    .bg-dark {
    background-color: #7fbfff !important;
}
</style>

<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenido a SaludPlus</h1>

        <p class="lead">Esta aplicación te permite gestionar la información de los centros de salud.</p>


    </div>

  <div class="body-content">

    <div class="row">
        <div class="col-lg-6">
            <div class="jumbotron text-center bg-transparent" style="background-color: rgba(0, 0, 255, 0.5); border-radius: 15px;">
                <h2>Pacientes</h2>
                <p class="lead">Esta aplicación te permite gestionar pacientes.</p>
                <?= Html::a('Gestionar Pacientes', ['pacientes/index'], ['class' => 'btn btn-lg btn-success']) ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="jumbotron text-center bg-transparent" style="background-color: rgba(0, 0, 255, 0.5); border-radius: 15px;">
                <h2>Médicos</h2>
                <p class="lead">Los médicos tienen su propia sección.</p>
                <?= Html::a('Ver Médicos', ['medicos/index'], ['class' => 'btn btn-lg btn-success']) ?>
            </div>
        </div>
    </div>

</div>

    </div>
</div>
