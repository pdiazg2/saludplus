<?php

use app\models\ModeloProductos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Modelo Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Modelo Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProductos',
            'nombre',
            'precio',
            'area',
            'limitada',
            //'stock',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ModeloProductos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idProductos' => $model->idProductos]);
                 }
            ],
        ],
    ]); ?>


</div>
