<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */

$this->title = 'Create Modelo Productos';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-productos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
