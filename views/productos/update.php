<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */

$this->title = 'Update Modelo Productos: ' . $model->idProductos;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idProductos, 'url' => ['view', 'idProductos' => $model->idProductos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
