<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ModeloProductos $model */

$this->title = $model->idProductos;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modelo-productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idProductos' => $model->idProductos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idProductos' => $model->idProductos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProductos',
            'nombre',
            'precio',
            'area',
            'limitada',
            'stock',
        ],
    ]) ?>

</div>
