<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */

$this->title = 'Create Modelo Utilizan';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Utilizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-utilizan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
