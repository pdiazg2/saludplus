<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-utilizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPaciente')->textInput() ?>

    <?= $form->field($model, 'idProductos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
