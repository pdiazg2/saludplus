<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloUtilizan $model */

$this->title = 'Update Modelo Utilizan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Utilizans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-utilizan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
