<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProporcionan $model */

$this->title = 'Create Modelo Proporcionan';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Proporcionans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-proporcionan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
