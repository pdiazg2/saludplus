<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProporcionan $model */

$this->title = 'Update Modelo Proporcionan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Proporcionans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-proporcionan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
