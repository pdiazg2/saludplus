<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-procedimientos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numExpediente')->textInput() ?>

    <?= $form->field($model, 'detalles')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idPaciente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
