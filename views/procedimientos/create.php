<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloProcedimientos $model */

$this->title = 'Create Modelo Procedimientos';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Procedimientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-procedimientos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
