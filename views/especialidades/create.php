<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloEspecialidades $model */

$this->title = 'Create Modelo Especialidades';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Especialidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-especialidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
