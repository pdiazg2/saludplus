<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelo-telefonosmedico-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idMedico')->textInput() ?>

    <?= $form->field($model, 'telefonosMedico')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
