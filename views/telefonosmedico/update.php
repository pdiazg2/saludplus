<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */

$this->title = 'Update Modelo Telefonosmedico: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Modelo Telefonosmedicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelo-telefonosmedico-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
