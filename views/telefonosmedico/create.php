<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ModeloTelefonosmedico $model */

$this->title = 'Create Modelo Telefonosmedico';
$this->params['breadcrumbs'][] = ['label' => 'Modelo Telefonosmedicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelo-telefonosmedico-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
