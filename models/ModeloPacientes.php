<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacientes".
 *
 * @property int $id
 * @property string $dni
 * @property string|null $direccion
 * @property string $apellido
 * @property string $nombre
 * @property string|null $expediente
 * @property int $idMedico
 * @property string|null $fechaDeConsulta
 *
 * @property Medicos $idMedico0
 * @property Productos[] $idProductos
 * @property Procedimientos[] $procedimientos
 * @property Telefonospaciente $telefonospaciente
 * @property Utilizan[] $utilizans
 */
class ModeloPacientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pacientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'apellido', 'nombre', 'idMedico'], 'required'],
            [['idMedico'], 'integer'],
            [['fechaDeConsulta'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['direccion', 'apellido'], 'string', 'max' => 50],
            [['nombre'], 'string', 'max' => 20],
            [['expediente'], 'string', 'max' => 500],
            [['dni'], 'unique'],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'Dni',
            'direccion' => 'Direccion',
            'apellido' => 'Apellido',
            'nombre' => 'Nombre',
            'expediente' => 'Expediente',
            'idMedico' => 'Id Medico',
            'fechaDeConsulta' => 'Fecha De Consulta',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0()
    {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }

    /**
     * Gets query for [[IdProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos()
    {
        return $this->hasMany(Productos::class, ['idProductos' => 'idProductos'])->viaTable('utilizan', ['idPaciente' => 'id']);
    }

    /**
     * Gets query for [[Procedimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProcedimientos()
    {
        return $this->hasMany(Procedimientos::class, ['idPaciente' => 'id']);
    }

    /**
     * Gets query for [[Telefonospaciente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonospaciente()
    {
        return $this->hasOne(Telefonospaciente::class, ['idPaciente' => 'id']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['idPaciente' => 'id']);
    }
}
