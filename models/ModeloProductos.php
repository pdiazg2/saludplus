<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $idProductos
 * @property string $nombre
 * @property int $precio
 * @property string|null $area
 * @property int $limitada
 * @property int|null $stock
 *
 * @property Medicos[] $idMedicos
 * @property Pacientes[] $idPacientes
 * @property Proveedores[] $idProveedors
 * @property Proporcionan[] $proporcionans
 * @property Recetan[] $recetans
 * @property Utilizan[] $utilizans
 */
class ModeloProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'precio', 'limitada'], 'required'],
            [['precio', 'limitada', 'stock'], 'integer'],
            [['nombre', 'area'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProductos' => 'Id Productos',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'area' => 'Area',
            'limitada' => 'Limitada',
            'stock' => 'Stock',
        ];
    }

    /**
     * Gets query for [[IdMedicos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedicos()
    {
        return $this->hasMany(Medicos::class, ['id' => 'idMedico'])->viaTable('recetan', ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[IdPacientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes()
    {
        return $this->hasMany(Pacientes::class, ['id' => 'idPaciente'])->viaTable('utilizan', ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[IdProveedors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProveedors()
    {
        return $this->hasMany(Proveedores::class, ['id' => 'idProveedor'])->viaTable('proporcionan', ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[Proporcionans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProporcionans()
    {
        return $this->hasMany(Proporcionan::class, ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[Recetans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecetans()
    {
        return $this->hasMany(Recetan::class, ['idProductos' => 'idProductos']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['idProductos' => 'idProductos']);
    }
}
