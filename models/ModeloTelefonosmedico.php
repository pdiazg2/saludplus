<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonosmedico".
 *
 * @property int $id
 * @property int $idMedico
 * @property string|null $telefonosMedico
 *
 * @property Medicos $idMedico0
 */
class ModeloTelefonosmedico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonosmedico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMedico'], 'required'],
            [['idMedico'], 'integer'],
            [['telefonosMedico'], 'string', 'max' => 20],
            [['idMedico'], 'unique'],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMedico' => 'Id Medico',
            'telefonosMedico' => 'Telefonos Medico',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0()
    {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }
}
