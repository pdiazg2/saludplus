<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonospaciente".
 *
 * @property int $id
 * @property int $idPaciente
 * @property string|null $telefonosPaciente
 *
 * @property Pacientes $idPaciente0
 */
class ModeloTelefonospaciente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonospaciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPaciente'], 'required'],
            [['idPaciente'], 'integer'],
            [['telefonosPaciente'], 'string', 'max' => 20],
            [['idPaciente'], 'unique'],
            [['idPaciente'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPaciente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPaciente' => 'Id Paciente',
            'telefonosPaciente' => 'Telefonos Paciente',
        ];
    }

    /**
     * Gets query for [[IdPaciente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPaciente0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPaciente']);
    }
}
