<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "especialidades".
 *
 * @property int $id
 * @property int $idMedico
 * @property string|null $especialidades
 *
 * @property Medicos $idMedico0
 */
class ModeloEspecialidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'especialidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMedico'], 'required'],
            [['idMedico'], 'integer'],
            [['especialidades'], 'string', 'max' => 20],
            [['idMedico', 'especialidades'], 'unique', 'targetAttribute' => ['idMedico', 'especialidades']],
            [['idMedico'], 'exist', 'skipOnError' => true, 'targetClass' => Medicos::class, 'targetAttribute' => ['idMedico' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMedico' => 'Id Medico',
            'especialidades' => 'Especialidades',
        ];
    }

    /**
     * Gets query for [[IdMedico0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMedico0()
    {
        return $this->hasOne(Medicos::class, ['id' => 'idMedico']);
    }
}
